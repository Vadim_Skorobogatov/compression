import matplotlib.pyplot as plt
import numpy as np
class ff(object):
    """docstring for """
    def __init__(self, delta,consumer,t_0,y_0,alpha):
        self.t_0 = t_0
        self.t_max = t_0
        self.y_0 = y_0        
        self.delta = delta
        self.delta1 = delta
        self.consumer = consumer
        self.set_unlim()
        self.alpha = alpha

    def set_unlim(self):
        self.kmin=-1e308
        self.kmax=1e308

    def __call__(self,t,y):
        # self.consumer.append()
        dt = t-self.t_0

        # print(self.delta1)
        kmin = (y - self.y_0 - self.delta1)/dt
        kmax = (y - self.y_0 + self.delta1)/dt
        if kmin > self.kmax or kmax < self.kmin:
            self.delta1 = self.delta - self.alpha * abs(self.y_0 - y)
            best_k = (self.kmin + self.kmax)/2
            ddt = self.t_max - self.t_0
            ynew = self.y_0 + best_k *ddt
            self.consumer.append((self.t_0,self.y_0))
            self.t_0 = self.t_max
            self.y_0 = ynew
            self.set_unlim()
            self(t,y)
        else:
            self.kmin = max(self.kmin,kmin)
            self.kmax = min(self.kmax,kmax)
            self.t_max = t

    def stop(self):
        self.consumer.append((self.t_0,self.y_0))
        best_k = (self.kmin + self.kmax) / 2
        ddt = self.t_max - self.t_0
        ynew = self.y_0 + best_k * ddt
        self.consumer.append((self.t_max, ynew))


data = np.loadtxt(r'E:\Vadim\projects\test_electro\test\Test3\GCN_ON\data\N.TXT' , skiprows = 0).T
time = data[0][500000:800000]
val = data[1][500000:800000]


dd = list(zip(time[1:],val[1:]))
fig, ax = plt.subplots()
# plt.plot(time,val,label = "data")
al = []
compr = []

for i in np.arange(0,2,0.05):
    consumer = []
    alpha = i
    ffilter = ff(10,consumer,time[0],val[0],alpha)
    for t, v in dd:
        ffilter(t, v)
    ffilter.stop()
    valf = [i[1] for i in consumer]
    timf = [i[0] for i in consumer]
    # plt.plot(timf,valf,label = "alpha = {} ({:.2f})".format(alpha,len(dd)/len(consumer)))
    # plt.plot(timf,range(len(timf)),label = "alpha = {}".format(alpha))
    al.append(alpha)
    compr.append(len(dd)/len(consumer))

plt.plot(al,compr)
ax.legend()
plt.grid()
plt.show()