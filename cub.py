import matplotlib.pyplot as plt
import numpy as np
class ff(object):
    """docstring for """
    def __init__(self, delta,consumer,t_0,y_0,alpha):
        self.t_0 = t_0
        self.t_max = t_0
        self.y_0 = y_0
        self.delta = delta
        self.consumer = consumer
        self.set_unlim()
        self.alpha = alpha

    def set_unlim(self):
        self.cmin=-1e308
        self.dmin = -1e308
        self.cmax=1e308
        self.dmax = 1e308

    def __call__(self,t,y):
        # self.consumer.append()
        dt = t-self.t_0

        a = self.y_0
        b = (self.y_0 - y) / dt
        self.b = (self.y_0 - y) / dt
        self.delta1 = self.delta-self.alpha*abs(self.y_0-y)

        cmax = (3*(-a + self.delta + b * self.t_0 - b * t + y)) / ((self.t_0 - t)**2)
        dmax = (2*(-a - self.delta + b * self.t_0 - b * t + y)) / ((self.t_0 - t)**3)

        cmin = (3*(-a - self.delta + b * self.t_0 - b * t + y)) / ((self.t_0 - t) ** 2)
        dmin = (2*(-a + self.delta + b * self.t_0 - b * t + y)) / ((self.t_0 - t) ** 3)

        if (cmin > self.cmax and dmin > self.dmax) or (cmax < self.cmin and dmax < self.dmin):
            best_c = (cmax+cmin)/2
            best_d = (dmax+dmin)/2
            ddt = self.t_max - self.t_0
            ynew = self.y_0 + b * ddt + best_c * ddt * ddt + best_d * ddt**3
            self.consumer.append((a,b,best_c,best_d,self.t_0))
            self.t_0 = self.t_max
            self.y_0 = ynew
            self.set_unlim()
            self(t,y)
        else:
            self.cmin = max(self.cmin,cmin)
            self.cmax = min(self.cmax,cmax)
            self.dmin = max(self.dmin, dmin)
            self.dmax = min(self.dmax, dmax)
            self.t_max = t

    def stop(self):
        self.consumer.append((self.t_0,self.y_0))
        best_c = (self.cmax + self.cmin) / 2
        best_d = (self.dmax + self.dmin) / 2
        ddt = self.t_max - self.t_0
        ynew = self.y_0 + self.b * ddt + best_c * ddt * ddt + best_d * ddt ** 3
        self.consumer.append((self.y_0, self.b, best_c, best_d, self.t_0))


data = np.loadtxt(r'E:\Vadim\projects\test_electro\test\Test3\GCN_ON\data\Nsh.TXT' , skiprows = 0).T
time = data[0][1:10]
val = data[1][1:10]

dd = list(zip(time[1:],val[1:]))
fig, ax = plt.subplots()
plt.plot(time,val,label = "data")

consumer = []
alpha = 0.9
ffilter = ff(10,consumer,time[0],val[0],alpha)
for t, v in dd:
    ffilter(t, v)
ffilter.stop()
valf = [i[1] for i in consumer]
timf = [i[0] for i in consumer]
plt.plot(timf,valf,label = "alpha = {} ({:.2f})".format(alpha,len(dd)/len(consumer)))

ax.legend()
plt.grid()
plt.show()
